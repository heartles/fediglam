const std = @import("std");
const http = @import("./lib.zig");

const parser = @import("./request/parser.zig");

pub fn Request(comptime Reader: type) type {
    return struct {
        protocol: http.Protocol,

        method: http.Method,
        uri: []const u8,
        headers: http.Fields,

        body: ?parser.TransferStream(Reader),

        pub fn parseFree(self: *@This(), allocator: std.mem.Allocator) void {
            allocator.free(self.uri);
            self.headers.deinit();
        }
    };
}

pub fn parse(alloc: std.mem.Allocator, reader: anytype) !Request(@TypeOf(reader)) {
    return parser.parse(alloc, reader);
}
