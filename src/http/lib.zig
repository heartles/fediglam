const std = @import("std");

const request = @import("./request.zig");
const server = @import("./server.zig");
pub const urlencode = @import("./urlencode.zig");
pub const socket = @import("./socket.zig");
const json = @import("./json.zig");
const multipart = @import("./multipart.zig");
pub const fields = @import("./fields.zig");

pub const Method = enum {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH,

    // WebDAV methods (we use some of them for the drive system)
    MKCOL,
    MOVE,

    pub fn requestHasBody(self: Method) bool {
        return switch (self) {
            .POST, .PUT, .PATCH, .MKCOL, .MOVE => true,
            else => false,
        };
    }
};

pub const Status = std.http.Status;

pub const Request = request.Request(server.Stream.Reader);
pub const Response = server.Response;
//pub const Handler = server.Handler;
pub const Server = server.Server;

pub const middleware = @import("./middleware.zig");

pub const Fields = fields.Fields;

pub const FormFile = multipart.FormFile;

pub const Protocol = enum {
    http_1_0,
    http_1_1,
    http_1_x,
};

test {
    _ = std.testing.refAllDecls(@This());
}
