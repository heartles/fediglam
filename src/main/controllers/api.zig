const controllers = @import("../controllers.zig");

const auth = @import("./api/auth.zig");
const communities = @import("./api/communities.zig");
const drive = @import("./api/drive.zig");
const invites = @import("./api/invites.zig");
const users = @import("./api/users.zig");
const follows = @import("./api/users/follows.zig");
const notes = @import("./api/notes.zig");
const streaming = @import("./api/streaming.zig");
const timelines = @import("./api/timelines.zig");

pub const routes = .{
    controllers.apiEndpoint(auth.login),
    controllers.apiEndpoint(auth.verify_login),
    controllers.apiEndpoint(communities.create),
    controllers.apiEndpoint(communities.query),
    controllers.apiEndpoint(invites.create),
    controllers.apiEndpoint(users.create),
    controllers.apiEndpoint(users.get),
    controllers.apiEndpoint(users.update_profile),
    controllers.apiEndpoint(notes.create),
    controllers.apiEndpoint(notes.get),
    //controllers.apiEndpoint(streaming.streaming),
    controllers.apiEndpoint(timelines.global),
    controllers.apiEndpoint(timelines.local),
    controllers.apiEndpoint(timelines.home),
    controllers.apiEndpoint(follows.create),
    controllers.apiEndpoint(follows.delete),
    controllers.apiEndpoint(follows.query_followers),
    controllers.apiEndpoint(follows.query_following),
    controllers.apiEndpoint(drive.upload),
    controllers.apiEndpoint(drive.mkdir),
    controllers.apiEndpoint(drive.get),
    controllers.apiEndpoint(drive.delete),
    controllers.apiEndpoint(drive.move),
    controllers.apiEndpoint(drive.update),
};
