const api = @import("api");
const util = @import("util");
const controller_utils = @import("../../../controllers.zig").helpers;

const Uuid = util.Uuid;

pub const create = struct {
    pub const method = .POST;
    pub const path = "/users/:id/follow";

    pub const Args = struct {
        id: Uuid,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        try srv.follow(req.args.id);

        try res.json(.created, .{});
    }
};

pub const delete = struct {
    pub const method = .DELETE;
    pub const path = "/users/:id/follow";

    pub const Args = struct {
        id: Uuid,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        try srv.unfollow(req.args.id);

        try res.json(.ok, .{});
    }
};

pub const query_followers = struct {
    pub const method = .GET;
    pub const path = "/users/:id/followers";

    pub const Args = struct {
        id: Uuid,
    };

    pub const Query = api.follows.FollowingQueryArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const results = try srv.queryFollowers(req.args.id, req.query);

        try controller_utils.paginate(results, res, req.allocator);
    }
};

pub const query_following = struct {
    pub const method = .GET;
    pub const path = "/users/:id/following";

    pub const Args = struct {
        id: Uuid,
    };

    pub const Query = api.follows.FollowerQueryArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const results = try srv.queryFollowing(req.args.id, req.query);

        try controller_utils.paginate(results, res, req.allocator);
    }
};
