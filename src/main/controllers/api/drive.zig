const std = @import("std");
const api = @import("api");
const http = @import("http");
const util = @import("util");
const controller_utils = @import("../../controllers.zig").helpers;

const Uuid = util.Uuid;
const DateTime = util.DateTime;

pub const drive_path = "/drive/:path*";
pub const DriveArgs = struct {
    path: []const u8,
};

pub const get = struct {
    pub const method = .GET;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const result = try srv.driveGet(req.args.path);
        defer util.deepFree(srv.allocator, result);

        try res.json(.ok, result);
    }
};

pub const upload = struct {
    pub const method = .POST;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    pub const Body = struct {
        file: http.FormFile,
        description: ?[]const u8 = null,
        sensitive: bool = false,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const f = req.body.file;
        const result = try srv.driveUpload(.{
            .dir = req.args.path,
            .filename = f.filename,
            .description = req.body.description,
            .content_type = f.content_type,
            .sensitive = req.body.sensitive,
        }, f.data);
        errdefer util.deepFree(srv.allocator, result);

        try res.json(.created, result);
    }
};

pub const delete = struct {
    pub const method = .DELETE;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        try srv.driveDelete(req.args.path);

        return res.json(.ok, .{});
    }
};

pub const mkdir = struct {
    pub const method = .MKCOL;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        var split = std.mem.splitBackwards(u8, std.mem.trim(u8, req.args.path, "/"), "/");
        const name = split.first();
        const parent = split.rest();
        std.log.debug("{s}, {s}", .{ parent, name });

        const result = try srv.driveMkdir(parent, name);
        errdefer util.deepFree(srv.allocator, result);

        try res.json(.created, result);
    }
};

pub const update = struct {
    pub const method = .PUT;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    // TODO: Validate that unhandled fields are equivalent to ones in the object
    pub const allow_unknown_fields_in_body = true;
    pub const Body = struct {
        meta: struct {
            filename: ?[]const u8 = null,
            description: ?[]const u8 = null,
            content_type: ?[]const u8 = null,
            sensitive: ?bool = null,
        },
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        try srv.driveUpdate(req.args.path, .{
            .filename = req.body.meta.filename,
            .description = req.body.meta.description,
            .content_type = req.body.meta.content_type,
            .sensitive = req.body.meta.sensitive,
        });

        const result = try srv.driveGet(req.args.path);
        defer util.deepFree(srv.allocator, result);
        try res.json(.ok, result);
    }
};

pub const move = struct {
    pub const method = .MOVE;
    pub const path = drive_path;
    pub const Args = DriveArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const destination = req.headers.get("Destination") orelse return error.NoDestination;

        try srv.driveMove(req.args.path, destination);

        const result = try srv.driveGet(req.args.path);
        defer util.deepFree(srv.allocator, result);

        try res.headers.put("Location", destination);
        try res.json(.created, result);
    }
};
