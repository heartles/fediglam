const std = @import("std");
const api = @import("api");
const controller_utils = @import("../../controllers.zig").helpers;

const TimelineArgs = api.timelines.TimelineArgs;

pub const global = struct {
    pub const method = .GET;
    pub const path = "/timelines/global";

    pub const Query = TimelineArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const results = try srv.globalTimeline(req.query);
        try controller_utils.paginate(results, res, req.allocator);
    }
};

pub const local = struct {
    pub const method = .GET;
    pub const path = "/timelines/local";

    pub const Query = TimelineArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const results = try srv.localTimeline(req.query);
        try controller_utils.paginate(results, res, req.allocator);
    }
};

pub const home = struct {
    pub const method = .GET;
    pub const path = "/timelines/home";

    pub const Query = TimelineArgs;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const results = try srv.homeTimeline(req.query);
        try controller_utils.paginate(results, res, req.allocator);
    }
};
