const api = @import("api");
const util = @import("util");
const controller_utils = @import("../../controllers.zig").helpers;

const QueryArgs = api.communities.QueryArgs;

pub const create = struct {
    pub const method = .POST;
    pub const path = "/communities";

    pub const Body = struct {
        origin: []const u8,
        name: ?[]const u8 = null,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const invite = try srv.createCommunity(req.body.origin, req.body.name);

        try res.json(.created, invite);
    }
};

pub const query = struct {
    pub const method = .GET;
    pub const path = "/communities";

    pub const Query = struct {
        const OrderBy = QueryArgs.OrderBy;
        const Direction = QueryArgs.Direction;
        const PageDirection = QueryArgs.PageDirection;

        // Max items to fetch
        max_items: usize = 20,

        // Selection filters
        owner_id: ?util.Uuid = null,
        like: ?[]const u8 = null,
        created_before: ?util.DateTime = null,
        created_after: ?util.DateTime = null,

        // Ordering parameter
        order_by: OrderBy = .created_at,
        direction: Direction = .ascending,

        // Page start parameter
        prev: ?union(OrderBy) {
            name: struct {
                id: util.Uuid,
                name: []const u8,
            },
            host: struct {
                id: util.Uuid,
                host: []const u8,
            },
            created_at: struct {
                id: util.Uuid,
                created_at: util.DateTime,
            },
        } = null,

        page_direction: PageDirection = .forward,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const q = req.query;
        const results = try srv.queryCommunities(.{
            .max_items = q.max_items,
            .owner_id = q.owner_id,
            .like = q.like,
            .created_before = q.created_before,
            .created_after = q.created_after,
            .order_by = q.order_by,
            .direction = q.direction,
            .prev = if (q.prev) |prev| switch (prev) {
                .name => |p| .{ .id = p.id, .order_val = .{ .name = p.name } },
                .host => |p| .{ .id = p.id, .order_val = .{ .host = p.host } },
                .created_at => |p| .{ .id = p.id, .order_val = .{ .created_at = p.created_at } },
            } else null,
            .page_direction = q.page_direction,
        });

        const convert = struct {
            fn func(args: QueryArgs) Query {
                return .{
                    .max_items = args.max_items,
                    .owner_id = args.owner_id,
                    .like = args.like,
                    .created_before = args.created_before,
                    .created_after = args.created_after,
                    .order_by = args.order_by,
                    .direction = args.direction,
                    .prev = if (args.prev) |prev| switch (prev.order_val) {
                        .name => |v| .{ .name = .{ .id = prev.id, .name = v } },
                        .host => |v| .{ .host = .{ .id = prev.id, .host = v } },
                        .created_at => |v| .{ .created_at = .{ .id = prev.id, .created_at = v } },
                    } else null,
                    .page_direction = args.page_direction,
                };
            }
        }.func;

        try controller_utils.paginate(.{
            .items = results.items,
            .next_page = convert(results.next_page),
            .prev_page = convert(results.prev_page),
        }, res, req.allocator);
    }
};
