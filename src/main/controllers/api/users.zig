const util = @import("util");
const api = @import("api");

pub const create = struct {
    pub const method = .POST;
    pub const path = "/users";

    pub const Body = struct {
        username: []const u8,
        password: []const u8,
        invite_code: ?[]const u8 = null,
        email: ?[]const u8 = null,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const options = .{
            .username = req.body.username,
            .password = req.body.password,
            .invite_code = req.body.invite_code,
            .email = req.body.email,
        };
        const user = srv.register(options) catch |err| switch (err) {
            error.UsernameTaken => return res.err(.unprocessable_entity, "Username Unavailable", {}),
            else => return err,
        };

        try res.json(.created, user);
    }
};

pub const get = struct {
    pub const method = .GET;
    pub const path = "/users/:id";

    pub const Args = struct {
        id: util.Uuid,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const result = try srv.getActor(req.args.id);
        defer util.deepFree(srv.allocator, result);

        try res.json(.ok, result);
    }
};

pub const update_profile = struct {
    pub const method = .PUT;
    pub const path = "/users/:id";

    pub const Args = struct {
        id: util.Uuid,
    };

    pub const Body = api.actors.ProfileUpdateArgs;

    // TODO: I don't like that the request body and response body are substantially different
    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        try srv.updateUserProfile(req.args.id, req.body);

        const result = try srv.getActor(req.args.id);
        defer util.deepFree(srv.allocator, result);

        try res.json(.ok, result);
    }
};
