const api = @import("api");

pub const create = struct {
    pub const method = .POST;
    pub const path = "/invites";

    pub const Body = api.invites.CreateOptions;

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        // No need to free because it will be freed when the api conn
        // is closed
        const invite = try srv.createInvite(req.body);

        try res.json(.created, invite);
    }
};
