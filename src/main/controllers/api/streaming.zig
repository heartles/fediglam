const http = @import("http");
const std = @import("std");

pub const streaming = struct {
    pub const method = .GET;
    pub const path = "/streaming";

    pub fn handler(req: anytype, response: anytype, _: anytype) !void {
        var iter = req.headers.iterator();
        std.log.debug("--Headers--", .{});
        while (iter.next()) |pair| {
            std.log.debug("{s}: {s}", .{ pair.key_ptr.*, pair.value_ptr.* });
        }

        const res = response.hijack();

        var socket = try http.socket.handshake(req.allocator, req.base_request, res);

        while (true) {
            var message = try socket.accept();
            defer message.close();
            std.log.debug("Message received", .{});

            const reader = message.reader();
            const msg = try reader.readAllAlloc(req.allocator, 1 << 63);
            defer req.allocator.free(msg);

            var response_msg = try socket.openMessage(.text, req.allocator, .{});
            defer response_msg.close();

            try response_msg.writer().writeAll(msg);
            try response_msg.finish();
            std.log.debug("{s}", .{msg});
        }
    }
};
