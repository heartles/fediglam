const api = @import("api");
const util = @import("util");

pub const create = struct {
    pub const method = .POST;
    pub const path = "/notes";

    pub const Body = struct {
        content: []const u8,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const note = try srv.createNote(req.body.content);

        try res.json(.created, note);
    }
};

pub const get = struct {
    pub const method = .GET;
    pub const path = "/notes/:id";

    pub const Args = struct {
        id: util.Uuid,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const note = try srv.getNote(req.args.id);

        try res.json(.ok, note);
    }
};
