const api = @import("api");
const std = @import("std");

pub const login = struct {
    pub const method = .POST;
    pub const path = "/auth/login";

    pub const Body = struct {
        username: []const u8,
        password: []const u8,
    };

    pub fn handler(req: anytype, res: anytype, srv: anytype) !void {
        const token = try srv.login(req.body.username, req.body.password);

        std.log.debug("{any}", .{res.headers});

        try res.json(.ok, token);
    }
};

pub const verify_login = struct {
    pub const method = .GET;
    pub const path = "/auth/login";

    pub fn handler(_: anytype, res: anytype, srv: anytype) !void {
        if (srv.context.token_info) |token| {
            return try res.json(.ok, token);
        } else {
            return try res.status(.unauthorized);
        }
    }
};
