const std = @import("std");
const util = @import("util");
const services = @import("../services.zig");
const pkg = @import("../lib.zig");

const Uuid = util.Uuid;
const ApiContext = pkg.ApiContext;

pub fn create(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    content: []const u8,
) !Uuid {
    // You cannot post on admin accounts
    if (ctx.community.kind == .admin) return error.WrongCommunity;

    // Only authenticated users can post
    const user_id = ctx.userId() orelse return error.TokenRequired;
    return try svcs.createNote(alloc, user_id, content);
}

pub fn get(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    note_id: Uuid,
) !pkg.Note {
    const note = try svcs.getNote(alloc, note_id);
    errdefer util.deepFree(alloc, note);

    // Only serve community-specific notes on unauthenticated requests
    if (ctx.userId() == null) {
        if (!Uuid.eql(ctx.community.id, note.author.community_id)) return error.NotFound;
    }

    return note;
}
