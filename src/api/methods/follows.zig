const std = @import("std");
const util = @import("util");
const pkg = @import("../lib.zig");
const services = @import("../services.zig");

const Uuid = util.Uuid;
const DateTime = util.DateTime;
const ApiContext = pkg.ApiContext;
const QueryArgs = services.follows.QueryArgs;
const FollowerQueryArgs = pkg.follows.FollowerQueryArgs;
const FollowingQueryArgs = pkg.follows.FollowingQueryArgs;

pub fn follow(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    followee: Uuid,
) !void {
    const user_id = ctx.userId() orelse return error.NoToken;
    try svcs.createFollow(alloc, user_id, followee);
}

pub fn unfollow(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    followee: Uuid,
) !void {
    const user_id = ctx.userId() orelse return error.NoToken;
    try svcs.deleteFollow(alloc, user_id, followee);
}

pub fn queryFollowers(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    of: Uuid,
    args: FollowerQueryArgs,
) !pkg.follows.FollowerQueryResult {
    const user = try svcs.getActor(alloc, of);
    defer util.deepFree(alloc, user);
    if (!Uuid.eql(user.community_id, ctx.community.id) and ctx.userId() == null) return error.NotFound;

    var all_args = std.mem.zeroInit(QueryArgs, args);
    all_args.followee_id = of;
    const result = try svcs.queryFollows(alloc, all_args);
    return .{
        .items = result.items,
        .prev_page = convert(FollowerQueryArgs, result.prev_page),
        .next_page = convert(FollowerQueryArgs, result.next_page),
    };
}

pub fn queryFollowing(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    of: Uuid,
    args: FollowingQueryArgs,
) !pkg.follows.FollowingQueryResult {
    const user = try svcs.getActor(alloc, of);
    defer util.deepFree(alloc, user);
    if (!Uuid.eql(user.community_id, ctx.community.id) and ctx.userId() == null) return error.NotFound;

    var all_args = std.mem.zeroInit(QueryArgs, args);
    all_args.followed_by_id = of;
    const result = try svcs.queryFollows(alloc, all_args);
    return .{
        .items = result.items,
        .prev_page = convert(FollowingQueryArgs, result.prev_page),
        .next_page = convert(FollowingQueryArgs, result.next_page),
    };
}

fn convert(comptime T: type, args: QueryArgs) T {
    return .{
        .max_items = args.max_items,
        .order_by = args.order_by,
        .direction = args.direction,
        .prev = args.prev,
        .page_direction = args.page_direction,
    };
}
