const std = @import("std");
const util = @import("util");
const types = @import("../types.zig");
const pkg = @import("../lib.zig");

const Uuid = util.Uuid;
const DateTime = util.DateTime;
const ApiContext = pkg.ApiContext;
const Community = types.communities.Community;
const QueryArgs = types.communities.QueryArgs;
const QueryResult = types.communities.QueryResult;

pub fn create(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    origin: []const u8,
    name: ?[]const u8,
) !Uuid {
    if (!ctx.isAdmin()) {
        return error.PermissionDenied;
    }

    return try svcs.createCommunity(
        alloc,
        origin,
        .{ .name = name },
    );
}

pub fn get(
    alloc: std.mem.Allocator,
    _: ApiContext,
    svcs: anytype,
    id: Uuid,
) !Community {
    return try svcs.getCommunity(alloc, id);
}

pub fn query(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    args: QueryArgs,
) !QueryResult {
    if (!ctx.isAdmin()) return error.PermissionDenied;
    return try svcs.queryCommunities(alloc, args);
}
