const std = @import("std");
const util = @import("util");
const services = @import("../services.zig");
const pkg = @import("../lib.zig");

const Uuid = util.Uuid;
const ApiContext = pkg.ApiContext;

const default_avatar = "static/default_avi.png";
pub fn get(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    id: Uuid,
) !pkg.Actor {
    const actor = try svcs.getActor(alloc, id);
    errdefer util.deepFree(alloc, actor);

    if (!Uuid.eql(actor.community_id, ctx.community.id) and ctx.userId() == null) {
        return error.NotFound;
    }

    const avatar_url = if (actor.avatar_file_id) |fid|
        try std.fmt.allocPrint(
            alloc,
            "{s}://{s}/media/{}",
            .{ @tagName(ctx.community.scheme), ctx.community.host, fid },
        )
    else
        try std.fmt.allocPrint(
            alloc,
            "{s}://{s}/{s}",
            .{ @tagName(ctx.community.scheme), ctx.community.host, default_avatar },
        );
    errdefer alloc.free(avatar_url);
    const header_url = if (actor.header_file_id) |fid|
        try std.fmt.allocPrint(
            alloc,
            "{s}://{s}/media/{}",
            .{ @tagName(ctx.community.scheme), ctx.community.host, fid },
        )
    else
        null;
    errdefer alloc.free(header_url);

    return pkg.Actor{
        .id = actor.id,

        .username = actor.username,
        .host = actor.host,

        .display_name = actor.display_name,
        .bio = actor.bio,

        .avatar_file_id = actor.avatar_file_id,
        .avatar_url = avatar_url,

        .header_file_id = actor.header_file_id,
        .header_url = header_url,

        .profile_fields = actor.profile_fields,

        .community_id = actor.community_id,

        .created_at = actor.created_at,
        .updated_at = actor.updated_at,
    };
}

pub fn updateProfile(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    id: Uuid,
    data: pkg.actors.ProfileUpdateArgs,
) !void {
    if (!Uuid.eql(id, ctx.userId() orelse return error.NoToken)) return error.AccessDenied;
    try svcs.updateActorProfile(alloc, id, data);
}
