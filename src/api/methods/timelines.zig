const std = @import("std");
const util = @import("util");
const pkg = @import("../lib.zig");
const types = @import("../types.zig");

const ApiContext = pkg.ApiContext;
const Uuid = util.Uuid;
const DateTime = util.DateTime;

const services = @import("../services.zig");
const Note = services.Note;
const QueryArgs = services.notes.QueryArgs;

const TimelineArgs = types.timelines.TimelineArgs;

fn timelineArgs(args: services.notes.QueryArgs) TimelineArgs {
    return .{
        .max_items = args.max_items,
        .created_before = args.created_before,
        .created_after = args.created_after,
        .prev = args.prev,
        .page_direction = args.page_direction,
    };
}

pub const TimelineResult = struct {
    items: []Note,

    prev_page: TimelineArgs,
    next_page: TimelineArgs,
};

pub fn globalTimeline(
    alloc: std.mem.Allocator,
    _: ApiContext,
    svcs: anytype,
    args: TimelineArgs,
) !TimelineResult {
    const all_args = std.mem.zeroInit(QueryArgs, args);
    const result = try svcs.queryNotes(alloc, all_args);
    return TimelineResult{
        .items = result.items,
        .prev_page = timelineArgs(result.prev_page),
        .next_page = timelineArgs(result.next_page),
    };
}

pub fn localTimeline(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    args: TimelineArgs,
) !TimelineResult {
    var all_args = std.mem.zeroInit(QueryArgs, args);
    all_args.community_id = ctx.community.id;
    const result = try svcs.queryNotes(alloc, all_args);
    return TimelineResult{
        .items = result.items,
        .prev_page = timelineArgs(result.prev_page),
        .next_page = timelineArgs(result.next_page),
    };
}

pub fn homeTimeline(
    alloc: std.mem.Allocator,
    ctx: ApiContext,
    svcs: anytype,
    args: TimelineArgs,
) !TimelineResult {
    if (ctx.userId() == null) return error.NoToken;

    var all_args = std.mem.zeroInit(QueryArgs, args);
    all_args.followed_by = ctx.userId();
    const result = try svcs.queryNotes(alloc, all_args);
    return TimelineResult{
        .items = result.items,
        .prev_page = timelineArgs(result.prev_page),
        .next_page = timelineArgs(result.next_page),
    };
}
