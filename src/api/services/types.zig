const util = @import("util");

const Uuid = util.Uuid;
const DateTime = util.DateTime;

const common = struct {
    const Direction = enum {
        ascending,
        descending,

        pub const jsonStringify = util.jsonSerializeEnumAsString;
    };

    const PageDirection = enum {
        forward,
        backward,

        pub const jsonStringify = util.jsonSerializeEnumAsString;
    };

    fn QueryResult(comptime R: type, comptime A: type) type {
        return struct {
            items: []R,

            next_page: A,
            prev_page: A,
        };
    }
};

pub const accounts = struct {
    pub const Role = enum {
        user,
        admin,
    };

    pub const CreateArgs = struct {
        for_actor: Uuid,
        password_hash: []const u8,
        invite_id: ?Uuid = null,
        email: ?[]const u8 = null,
        role: Role = .user,
    };

    pub const Credentials = struct {
        account_id: Uuid,
        password_hash: []const u8,
    };
};

pub const actors = struct {
    pub const Actor = struct {
        id: Uuid,

        username: []const u8,
        host: []const u8,
        community_id: Uuid,

        display_name: ?[]const u8,
        bio: []const u8,

        avatar_file_id: ?Uuid,
        header_file_id: ?Uuid,

        profile_fields: []const ProfileField,

        created_at: DateTime,
        updated_at: DateTime,

        pub const sql_serialize = struct {
            pub const profile_fields = .json;
        };
    };

    pub const ProfileField = struct {
        key: []const u8,
        value: []const u8,
    };

    // TODO: get rid of this
    pub const Profile = struct {
        display_name: ?[]const u8,
        bio: []const u8,
        avatar_file_id: ?Uuid,
        header_file_id: ?Uuid,
        profile_fields: []const ProfileField,

        pub const sql_serialize = struct {
            pub const profile_fields = .json;
        };
    };

    pub const ProfileUpdateArgs = struct {
        display_name: ??[]const u8,
        bio: ?[]const u8,
        avatar_file_id: ??Uuid,
        header_file_id: ??Uuid,
        profile_fields: ?[]const ProfileField,

        pub const sql_serialize = struct {
            pub const profile_fields = .json;
        };
    };
};

pub const communities = struct {
    pub const Community = struct {
        id: Uuid,

        owner_id: ?Uuid,
        host: []const u8,
        name: []const u8,

        scheme: Scheme,
        kind: Kind,
        created_at: DateTime,
    };

    pub const Kind = enum {
        admin,
        local,

        pub const jsonStringify = util.jsonSerializeEnumAsString;
    };

    pub const Scheme = enum {
        https,
        http,

        pub const jsonStringify = util.jsonSerializeEnumAsString;
    };

    pub const CreateOptions = struct {
        name: ?[]const u8 = null,
        kind: Kind = .local,
    };

    pub const QueryArgs = struct {
        pub const OrderBy = enum {
            name,
            host,
            created_at,

            pub const jsonStringify = util.jsonSerializeEnumAsString;
        };

        pub const Direction = common.Direction;
        pub const PageDirection = common.PageDirection;
        pub const Prev = struct {
            id: Uuid,
            order_val: OrderVal,
        };
        pub const OrderVal = union(OrderBy) {
            name: []const u8,
            host: []const u8,
            created_at: DateTime,
        };

        // Max items to fetch
        max_items: usize = 20,

        // Selection filters
        owner_id: ?Uuid = null, // searches for communities owned by this user
        like: ?[]const u8 = null, // searches for communities with host or name LIKE '%?%'
        created_before: ?DateTime = null,
        created_after: ?DateTime = null,

        // Ordering parameter
        order_by: OrderBy = .created_at,
        direction: Direction = .ascending,

        // Page start parameter(s)
        // This struct is a reference to the last value scanned
        // If prev is present, then prev.order_val must have the same tag as order_by
        // "prev" here refers to it being the previous value returned. It may be that
        // prev refers to the item directly after the results you are about to recieve,
        // if you are querying the previous page.
        prev: ?Prev = null,

        // What direction to scan the page window
        // If "forward", then "prev" is interpreted as the item directly before the items
        // to query, in the direction of "direction" above. If "backward", then the opposite
        page_direction: PageDirection = .forward,
    };

    pub const QueryResult = common.QueryResult(Community, QueryArgs);
};

pub const drive = struct {
    pub const DriveEntry = struct {
        pub const Kind = enum {
            dir,
            file,

            pub const jsonStringify = util.jsonSerializeEnumAsString;
        };

        id: Uuid,
        owner_id: Uuid,

        name: ?[]const u8,

        path: []const u8,
        parent_directory_id: ?Uuid,

        file_id: ?Uuid,
        kind: Kind,
    };
};

pub const files = struct {
    pub const FileUpload = struct {
        pub const Status = enum {
            uploading,
            uploaded,
            external,
            deleted,

            pub const jsonStringify = util.jsonSerializeEnumAsString;
        };

        id: Uuid,

        owner_id: Uuid,
        size: usize,

        filename: []const u8,
        description: ?[]const u8,
        content_type: ?[]const u8,
        sensitive: bool,

        status: Status,

        created_at: DateTime,
        updated_at: DateTime,
    };

    pub const CreateOptions = struct {
        filename: []const u8,
        description: ?[]const u8,
        content_type: ?[]const u8,
        sensitive: bool,
    };

    pub const UpdateArgs = struct {
        filename: ?[]const u8,
        description: ?[]const u8,
        content_type: ?[]const u8,
        sensitive: ?bool,
    };
};

pub const invites = struct {
    pub const UseCount = usize;
    pub const Invite = struct {
        id: Uuid,

        created_by: Uuid, // User ID
        community_id: Uuid,
        name: []const u8,
        code: []const u8,

        created_at: DateTime,
        times_used: UseCount,

        expires_at: ?DateTime,
        max_uses: ?UseCount,

        kind: Kind,
    };
    pub const Kind = enum {
        system,
        community_owner,
        user,

        pub const jsonStringify = util.jsonSerializeEnumAsString;
    };

    pub const CreateOptions = struct {
        created_by: Uuid,
        community_id: Uuid,
        name: ?[]const u8 = null,
        max_uses: ?UseCount = null,
        lifespan: ?DateTime.Duration = null,
        kind: Kind = .user,
    };
};

pub const follows = struct {
    pub const Follow = struct {
        id: Uuid,

        followed_by_id: Uuid,
        followee_id: Uuid,

        created_at: DateTime,
    };
    pub const QueryArgs = struct {
        pub const OrderBy = enum {
            created_at,
        };

        pub const Direction = common.Direction;
        pub const PageDirection = common.PageDirection;
        pub const Prev = struct {
            id: Uuid,
            order_val: union(OrderBy) {
                created_at: DateTime,
            },
        };

        max_items: usize = 20,

        followed_by_id: ?Uuid = null,
        followee_id: ?Uuid = null,

        order_by: OrderBy = .created_at,

        direction: Direction = .descending,

        prev: ?Prev = null,

        page_direction: PageDirection = .forward,
    };

    pub const QueryResult = common.QueryResult(Follow, QueryArgs);
};

pub const notes = struct {
    pub const Note = struct {
        id: Uuid,

        author: actors.Actor, // TODO
        content: []const u8,
        created_at: DateTime,

        // TODO: This sucks
        pub const sql_serialize = struct {
            pub const @"author.profile_fields" = .json;
        };
    };
    pub const QueryArgs = struct {
        pub const PageDirection = common.PageDirection;
        pub const Prev = struct {
            id: Uuid,
            created_at: DateTime,
        };

        max_items: usize = 20,

        created_before: ?DateTime = null,
        created_after: ?DateTime = null,
        community_id: ?Uuid = null,
        followed_by: ?Uuid = null,

        prev: ?Prev = null,

        page_direction: PageDirection = .forward,
    };
    pub const QueryResult = common.QueryResult(Note, QueryArgs);
};

pub const tokens = struct {
    pub const Token = struct {
        account_id: Uuid,
        issued_at: DateTime,

        hash: []const u8,
    };
};
