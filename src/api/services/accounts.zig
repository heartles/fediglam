const std = @import("std");
const util = @import("util");
const types = @import("./types.zig");

const Uuid = util.Uuid;
const DateTime = util.DateTime;

const CreateArgs = types.accounts.CreateArgs;
const Credentials = types.accounts.Credentials;

/// Creates a local account with the given information
pub fn create(
    db: anytype,
    args: CreateArgs,
    alloc: std.mem.Allocator,
) !void {
    const tx = try db.beginOrSavepoint();
    errdefer tx.rollback();

    tx.insert("account", .{
        .id = args.for_actor,
        .invite_id = args.invite_id,
        .email = args.email,
        .kind = args.role,
    }, alloc) catch return error.DatabaseFailure;
    tx.insert("password", .{
        .account_id = args.for_actor,
        .hash = args.password_hash,
        .changed_at = DateTime.now(),
    }, alloc) catch return error.DatabaseFailure;

    tx.commitOrRelease() catch return error.DatabaseFailure;
}

pub fn getCredentialsByUsername(db: anytype, username: []const u8, community_id: Uuid, alloc: std.mem.Allocator) !Credentials {
    return db.queryRow(
        Credentials,
        \\SELECT account.id as account_id, password.hash as password_hash
        \\FROM password
        \\      JOIN account
        \\      JOIN actor
        \\  ON password.account_id = account.id AND account.id = actor.id
        \\WHERE actor.username = $1
        \\  AND actor.community_id = $2
        \\LIMIT 1
    ,
        .{ username, community_id },
        alloc,
    ) catch |err| return switch (err) {
        error.NoRows => error.InvalidLogin,
        else => |e| return e,
    };
}
