const std = @import("std");
const util = @import("util");

pub const Direction = enum {
    ascending,
    descending,

    pub const jsonStringify = util.jsonSerializeEnumAsString;
};

pub const PageDirection = enum {
    forward,
    backward,

    pub const jsonStringify = util.jsonSerializeEnumAsString;
};

pub fn Partial(comptime T: type) type {
    const t_fields = std.meta.fields(T);
    var fields: [t_fields.len]std.builtin.Type.StructField = undefined;
    for (std.meta.fields(T)) |f, i| fields[i] = .{
        .name = f.name,
        .field_type = ?f.field_type,
        .default_value = &@as(?f.field_type, null),
        .is_comptime = false,
        .alignment = @alignOf(?f.field_type),
    };
    return @Type(.{ .Struct = .{
        .layout = .Auto,
        .fields = &fields,
        .decls = &.{},
        .is_tuple = false,
    } });
}

pub fn Subset(comptime T: type, comptime selected: []const std.meta.FieldEnum(T)) type {
    var fields: [selected.len]std.builtin.Type.StructField = undefined;
    for (selected) |f, i| fields[i] = .{
        .name = @tagName(f),
        .field_type = std.meta.fieldInfo(T, f).field_type,
        .default_value = @as(?std.meta.fieldInfo(T, f).field_type, null),
        .is_comptime = false,
        .alignment = @alignOf(std.meta.fieldInfo(T, f).field_type),
    };
    return @Type(.{ .Struct = .{
        .layout = .Auto,
        .fields = &fields,
        .decls = &.{},
        .is_tuple = false,
    } });
}
