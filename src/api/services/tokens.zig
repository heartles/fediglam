const std = @import("std");
const util = @import("util");

const Uuid = util.Uuid;
const DateTime = util.DateTime;

const Token = @import("./types.zig").tokens.Token;

pub fn create(db: anytype, account_id: Uuid, hash: []const u8, alloc: std.mem.Allocator) !void {
    const now = DateTime.now();
    try db.insert("token", .{
        .account_id = account_id,
        .hash = hash,
        .issued_at = now,
    }, alloc);
}

pub fn getByHash(db: anytype, hash: []const u8, community_id: Uuid, alloc: std.mem.Allocator) !Token {
    return db.queryRow(
        Token,
        \\SELECT account_id, issued_at, hash
        \\FROM token
        \\      JOIN account
        \\      JOIN actor
        \\  ON token.account_id = account.id AND account.id = actor.id
        \\WHERE token.hash = $1 AND actor.community_id = $2
        \\LIMIT 1
    ,
        .{ hash, community_id },
        alloc,
    ) catch |err| switch (err) {
        error.NoRows => error.InvalidToken,
        else => error.DatabaseFailure,
    };
}
