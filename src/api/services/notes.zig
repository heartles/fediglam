const std = @import("std");
const util = @import("util");
const sql = @import("sql");
const types = @import("./types.zig");

const Uuid = util.Uuid;
const DateTime = util.DateTime;
const Note = types.notes.Note;
const QueryArgs = types.notes.QueryArgs;
const QueryResult = types.notes.QueryResult;

pub const CreateError = error{
    DatabaseFailure,
};
pub fn create(
    db: anytype,
    author: Uuid,
    content: []const u8,
    alloc: std.mem.Allocator,
) CreateError!Uuid {
    const id = Uuid.randV4(util.getThreadPrng());

    db.insert("note", .{
        .id = id,
        .author_id = author,
        .content = content,
        .created_at = DateTime.now(),
    }, alloc) catch return error.DatabaseFailure;

    return id;
}

pub const GetError = error{
    DatabaseFailure,
    NotFound,
};
const selectStarFromNote = std.fmt.comptimePrint(
    \\SELECT {s}
    \\FROM note
    \\
, .{util.comptimeJoinWithPrefix(",", "note.", std.meta.fieldNames(Note))});
pub fn get(db: anytype, id: Uuid, alloc: std.mem.Allocator) GetError!Note {
    return db.queryRow(
        Note,
        \\SELECT
        \\  note.id,
        \\  note.content,
        \\  note.created_at,
        \\  actor.id AS "author.id",
        \\  actor.username AS "author.username",
        \\  community.host AS "author.host",
        \\  actor.display_name AS "author.display_name",
        \\  actor.bio AS "author.bio",
        \\  actor.avatar_file_id AS "author.avatar_file_id",
        \\  actor.header_file_id AS "author.header_file_id",
        \\  actor.profile_fields AS "author.profile_fields",
        \\  actor.community_id AS "author.community_id",
        \\  actor.created_at AS "author.created_at",
        \\  actor.updated_at AS "author.updated_at"
        \\FROM note
        \\  JOIN actor ON actor.id = note.author_id
        \\  JOIN community ON community.id = actor.community_id
        \\WHERE id = $1
        \\LIMIT 1
    ,
        .{id},
        alloc,
    ) catch |err| switch (err) {
        error.NoRows => error.NotFound,
        else => error.DatabaseFailure,
    };
}

const max_max_items = 100;

pub fn query(db: anytype, args: QueryArgs, alloc: std.mem.Allocator) !QueryResult {
    var builder = sql.QueryBuilder.init(alloc);
    defer builder.deinit();

    try builder.appendSlice(
        \\SELECT
        \\  note.id,
        \\  note.content,
        \\  note.created_at,
        \\  actor.id AS "author.id",
        \\  actor.username AS "author.username",
        \\  community.host AS "author.host",
        \\  actor.display_name AS "author.display_name",
        \\  actor.bio AS "author.bio",
        \\  actor.avatar_file_id AS "author.avatar_file_id",
        \\  actor.header_file_id AS "author.header_file_id",
        \\  actor.profile_fields AS "author.profile_fields",
        \\  actor.community_id AS "author.community_id",
        \\  actor.created_at AS "author.created_at",
        \\  actor.updated_at AS "author.updated_at"
        \\FROM note
        \\  JOIN actor ON actor.id = note.author_id
        \\  JOIN community ON community.id = actor.community_id
        \\
    );

    if (args.followed_by != null) try builder.appendSlice(
        \\  JOIN follow ON
        \\      follow.followed_by_id = $7 AND follow.followee_id = note.author_id
        \\
    );

    if (args.created_before != null) try builder.andWhere("note.created_at < $1");
    if (args.created_after != null) try builder.andWhere("note.created_at > $2");
    if (args.prev != null) {
        try builder.andWhere("(note.created_at, note.id)");

        switch (args.page_direction) {
            .forward => try builder.appendSlice(" < "),
            .backward => try builder.appendSlice(" > "),
        }
        try builder.appendSlice("($3, $4)");
    }
    if (args.community_id != null) try builder.andWhere("actor.community_id = $5");

    try builder.appendSlice(
        \\
        \\ORDER BY note.created_at DESC
        \\LIMIT $6
        \\
    );

    const max_items = if (args.max_items > max_max_items) max_max_items else args.max_items;

    const query_args = blk: {
        const prev_created_at = if (args.prev) |prev| @as(?DateTime, prev.created_at) else null;
        const prev_id = if (args.prev) |prev| @as(?Uuid, prev.id) else null;

        break :blk .{
            args.created_before,
            args.created_after,
            prev_created_at,
            prev_id,
            args.community_id,
            max_items,
            args.followed_by,
        };
    };

    const results = try db.queryRowsWithOptions(
        Note,
        try builder.terminate(),
        query_args,
        max_items,
        .{ .allocator = alloc, .ignore_unused_arguments = true },
    );
    errdefer util.deepFree(alloc, results);

    var next_page = args;
    var prev_page = args;
    prev_page.page_direction = .backward;
    next_page.page_direction = .forward;
    if (results.len != 0) {
        prev_page.prev = .{
            .id = results[0].id,
            .created_at = results[0].created_at,
        };

        next_page.prev = .{
            .id = results[results.len - 1].id,
            .created_at = results[results.len - 1].created_at,
        };
    }
    // TODO: this will give incorrect links on an empty page

    return QueryResult{
        .items = results,
        .next_page = next_page,
        .prev_page = prev_page,
    };
}
