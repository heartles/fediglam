const std = @import("std");
const util = @import("util");

const Uuid = util.Uuid;
const DateTime = util.DateTime;

const impl = struct {
    const communities = @import("./services/communities.zig");
    const actors = @import("./services/actors.zig");
    const drive = @import("./services/drive.zig");
    const files = @import("./services/files.zig");
    const invites = @import("./services/invites.zig");
    const notes = @import("./services/notes.zig");
    const follows = @import("./services/follows.zig");
    const accounts = @import("./services/accounts.zig");
    const tokens = @import("./services/tokens.zig");
};

const types = @import("./services/types.zig");
pub usingnamespace types;

pub const Account = types.accounts.Account;
pub const Credentials = types.accounts.Credentials;
pub const Actor = types.actors.Actor;
pub const Community = types.communities.Community;
pub const DriveEntry = types.drive.DriveEntry;
pub const FileUpload = types.files.FileUpload;
pub const Invite = types.invites.Invite;
pub const Note = types.notes.Note;
pub const Token = types.tokens.Token;

pub fn Services(comptime Db: type) type {
    return struct {
        const Self = @This();

        db: Db,

        pub fn beginTx(self: Self) !Services(Db.BeginOrSavepoint) {
            return Services(Db.BeginOrSavepoint){
                .db = try self.db.beginOrSavepoint(),
            };
        }

        pub fn commitTx(self: Self) !void {
            return try self.db.commitOrRelease();
        }

        pub fn rollbackTx(self: Self) void {
            return self.db.rollback();
        }

        pub fn createAccount(
            self: Self,
            alloc: std.mem.Allocator,
            args: types.accounts.CreateArgs,
        ) !void {
            return try impl.accounts.create(self.db, args, alloc);
        }

        pub fn getCredentialsByUsername(
            self: Self,
            alloc: std.mem.Allocator,
            username: []const u8,
            community_id: Uuid,
        ) !Credentials {
            return try impl.accounts.getCredentialsByUsername(self.db, username, community_id, alloc);
        }

        pub fn createActor(
            self: Self,
            alloc: std.mem.Allocator,
            username: []const u8,
            community_id: Uuid,
            lax_username: bool, // TODO: remove this
        ) !Uuid {
            return try impl.actors.create(self.db, username, community_id, lax_username, alloc);
        }

        pub fn getActor(
            self: Self,
            alloc: std.mem.Allocator,
            user_id: Uuid,
        ) !Actor {
            return try impl.actors.get(self.db, user_id, alloc);
        }

        pub fn lookupActorByUsername(
            self: Self,
            alloc: std.mem.Allocator,
            username: []const u8,
            community_id: Uuid,
        ) !Actor {
            return try impl.actors.lookupByUsername(self.db, username, community_id, alloc);
        }

        pub fn updateActorProfile(
            self: Self,
            alloc: std.mem.Allocator,
            actor_id: Uuid,
            new: types.actors.ProfileUpdateArgs,
        ) !void {
            return try impl.actors.updateProfile(self.db, actor_id, new, alloc);
        }

        pub fn createCommunity(
            self: Self,
            alloc: std.mem.Allocator,
            origin: []const u8,
            options: types.communities.CreateOptions,
        ) !Uuid {
            return try impl.communities.create(self.db, origin, options, alloc);
        }

        pub fn getCommunity(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) !Community {
            return try impl.communities.get(self.db, id, alloc);
        }

        pub fn getCommunityByHost(
            self: Self,
            alloc: std.mem.Allocator,
            host: []const u8,
        ) !Community {
            return try impl.communities.getByHost(self.db, host, alloc);
        }

        pub fn getAdminCommunityId(self: Self) !Uuid {
            return try impl.communities.adminCommunityId(self.db);
        }

        pub fn transferCommunityOwnership(self: Self, community_id: Uuid, owner_id: Uuid) !void {
            return try impl.communities.transferOwnership(self.db, community_id, owner_id);
        }

        pub fn queryCommunities(
            self: Self,
            alloc: std.mem.Allocator,
            args: types.communities.QueryArgs,
        ) !types.communities.QueryResult {
            return try impl.communities.query(self.db, args, alloc);
        }

        pub fn statDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            owner_id: Uuid,
            path: []const u8,
        ) !DriveEntry {
            return try impl.drive.stat(self.db, owner_id, path, alloc);
        }

        pub fn getDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) !DriveEntry {
            return try impl.drive.get(self.db, id, alloc);
        }

        pub fn createDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            owner_id: Uuid,
            containing_path: []const u8,
            name: []const u8,
            file_id: ?Uuid,
        ) !Uuid {
            return try impl.drive.create(self.db, owner_id, containing_path, name, file_id, alloc);
        }

        pub fn deleteDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            entry_id: Uuid,
        ) !void {
            return try impl.drive.delete(self.db, entry_id, alloc);
        }

        pub fn moveDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            owner_id: Uuid,
            src: []const u8,
            dest: []const u8,
        ) !void {
            return try impl.drive.move(self.db, owner_id, src, dest, alloc);
        }

        // TODO: paginate
        pub fn listDriveEntry(
            self: Self,
            alloc: std.mem.Allocator,
            entry_id: Uuid,
        ) ![]DriveEntry {
            return try impl.drive.list(self.db, entry_id, alloc);
        }

        pub fn createFile(
            self: Self,
            alloc: std.mem.Allocator,
            owner_id: Uuid,
            meta: types.files.CreateOptions,
            data: []const u8,
        ) !Uuid {
            return try impl.files.create(self.db, owner_id, meta, data, alloc);
        }

        pub fn deleteFile(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) !void {
            return try impl.files.delete(self.db, id, alloc);
        }

        pub fn statFile(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) !FileUpload {
            return try impl.files.get(self.db, id, alloc);
        }

        pub fn derefFile(
            _: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) ![]const u8 {
            return try impl.files.deref(alloc, id);
        }

        pub fn updateFileMetadata(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
            meta: types.files.UpdateArgs,
        ) !void {
            return try impl.files.update(self.db, id, meta, alloc);
        }

        pub fn createFollow(
            self: Self,
            alloc: std.mem.Allocator,
            followed_by: Uuid,
            followee: Uuid,
        ) !void {
            return try impl.follows.create(self.db, followed_by, followee, alloc);
        }

        pub fn deleteFollow(
            self: Self,
            alloc: std.mem.Allocator,
            followed_by: Uuid,
            followee: Uuid,
        ) !void {
            return try impl.follows.delete(self.db, followed_by, followee, alloc);
        }

        pub fn queryFollows(
            self: Self,
            alloc: std.mem.Allocator,
            args: types.follows.QueryArgs,
        ) !types.follows.QueryResult {
            return try impl.follows.query(self.db, args, alloc);
        }

        pub fn createInvite(
            self: Self,
            alloc: std.mem.Allocator,
            options: types.invites.CreateOptions,
        ) !Uuid {
            return try impl.invites.create(self.db, options, alloc);
        }

        pub fn getInvite(
            self: Self,
            alloc: std.mem.Allocator,
            invite_id: Uuid,
        ) !Invite {
            return try impl.invites.get(self.db, invite_id, alloc);
        }

        pub fn createNote(
            self: Self,
            alloc: std.mem.Allocator,
            author_id: Uuid,
            content: []const u8,
        ) !Uuid {
            return try impl.notes.create(self.db, author_id, content, alloc);
        }

        pub fn getNote(
            self: Self,
            alloc: std.mem.Allocator,
            id: Uuid,
        ) !Note {
            return try impl.notes.get(self.db, id, alloc);
        }

        pub fn queryNotes(
            self: Self,
            alloc: std.mem.Allocator,
            args: types.notes.QueryArgs,
        ) !types.notes.QueryResult {
            return try impl.notes.query(self.db, args, alloc);
        }

        pub fn getInviteByCode(
            self: Self,
            alloc: std.mem.Allocator,
            code: []const u8,
            community_id: Uuid,
        ) !Invite {
            return try impl.invites.getByCode(self.db, code, community_id, alloc);
        }

        pub fn createToken(
            self: Self,
            alloc: std.mem.Allocator,
            account_id: Uuid,
            hash: []const u8,
        ) !void {
            return try impl.tokens.create(self.db, account_id, hash, alloc);
        }

        pub fn getTokenByHash(
            self: Self,
            alloc: std.mem.Allocator,
            hash: []const u8,
            community_id: Uuid,
        ) !Token {
            return try impl.tokens.getByHash(self.db, hash, community_id, alloc);
        }
    };
}
