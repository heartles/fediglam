const util = @import("util");
const services = @import("./services.zig");

const Uuid = util.Uuid;
const DateTime = util.DateTime;

fn QueryResult(comptime R: type, comptime A: type) type {
    return struct {
        items: []R,

        next_page: A,
        prev_page: A,
    };
}

pub const auth = struct {
    pub const RegistrationOptions = struct {
        username: []const u8,
        password: []const u8,
        invite_code: ?[]const u8 = null,
        email: ?[]const u8 = null,
    };
};

pub const actors = struct {
    pub const Actor = struct {
        id: Uuid,

        username: []const u8,
        host: []const u8,

        display_name: ?[]const u8,
        bio: []const u8,

        avatar_file_id: ?Uuid,
        avatar_url: []const u8,

        header_file_id: ?Uuid,
        header_url: ?[]const u8,

        profile_fields: []const ProfileField,

        community_id: Uuid,

        created_at: DateTime,
        updated_at: DateTime,
    };
    pub const ProfileField = services.actors.ProfileField;
    pub const ProfileUpdateArgs = services.actors.ProfileUpdateArgs;
};

pub const communities = struct {
    pub const Community = services.communities.Community;
    pub const QueryArgs = services.communities.QueryArgs;
    pub const QueryResult = services.communities.QueryResult;
};

pub const drive = struct {
    pub const DriveEntry = union(enum) {
        pub const Kind = services.drive.DriveEntry.Kind;
        dir: struct {
            id: Uuid,
            owner_id: Uuid,
            name: ?[]const u8,
            path: []const u8,
            parent_directory_id: ?Uuid,

            kind: Kind = .dir,

            // If null = not enumerated
            children: ?[]const DriveEntry,
        },
        file: struct {
            id: Uuid,
            owner_id: Uuid,
            name: ?[]const u8,
            path: []const u8,
            parent_directory_id: ?Uuid,

            kind: Kind = .file,

            meta: files.FileUpload,
        },
    };

    pub const UploadArgs = struct {
        filename: []const u8,
        dir: []const u8,
        description: ?[]const u8,
        content_type: []const u8,
        sensitive: bool,
    };
};

pub const files = struct {
    pub const FileUpload = services.files.FileUpload;
    pub const UpdateArgs = services.files.UpdateArgs;

    pub const DerefResult = struct {
        meta: FileUpload,
        data: []const u8,
    };
};

pub const follows = struct {
    pub const Follow = services.follows.Follow;

    const QueryArgs = struct {
        pub const OrderBy = services.follows.QueryArgs.OrderBy;
        pub const Direction = services.follows.QueryArgs.Direction;
        pub const PageDirection = services.follows.QueryArgs.PageDirection;
        pub const Prev = services.follows.QueryArgs.Prev;

        max_items: usize = 20,

        order_by: OrderBy = .created_at,

        direction: Direction = .descending,

        prev: ?Prev = null,

        page_direction: PageDirection = .forward,
    };

    pub const FollowerQueryArgs = QueryArgs;
    pub const FollowingQueryArgs = QueryArgs;

    pub const FollowerQueryResult = QueryResult(Follow, FollowerQueryArgs);
    pub const FollowingQueryResult = QueryResult(Follow, FollowingQueryArgs);
};

pub const invites = struct {
    pub const UseCount = services.invites.UseCount;
    pub const Invite = struct {
        id: Uuid,

        created_by: Uuid, // User ID
        community_id: Uuid,
        name: []const u8,
        code: []const u8,
        url: []const u8,

        created_at: DateTime,
        times_used: UseCount,

        expires_at: ?DateTime,
        max_uses: ?UseCount,

        kind: Kind,
    };
    pub const Kind = services.invites.Kind;
    pub const CreateOptions = struct {
        name: ?[]const u8 = null,
        lifespan: ?DateTime.Duration = null,
        max_uses: ?usize = null,

        // admin only options
        kind: Kind = .user,
        to_community: ?Uuid = null,
    };
};

pub const notes = struct {
    pub const Note = services.notes.Note;
    pub const QueryArgs = services.notes.QueryArgs;
};

pub const timelines = struct {
    pub const TimelineArgs = struct {
        pub const PageDirection = notes.QueryArgs.PageDirection;
        pub const Prev = notes.QueryArgs.Prev;

        max_items: usize = 20,

        created_before: ?DateTime = null,
        created_after: ?DateTime = null,

        prev: ?Prev = null,

        page_direction: PageDirection = .forward,
    };

    pub const TimelineResult = struct {
        items: []notes.Note,

        prev_page: TimelineArgs,
        next_page: TimelineArgs,
    };
};

pub const tokens = struct {
    pub const Token = struct {
        pub const Info = struct {
            account_id: Uuid,
            issued_at: DateTime,
        };

        value: []const u8,
        info: Info,
    };
};
