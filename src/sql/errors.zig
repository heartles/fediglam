const std = @import("std");

// Error for any miscellaneous or unexpected error codes
const UnexpectedError = error{Unexpected};

// Errors related to database connections, common to (most) api calls
const ConnectionError = error{
    // The operation was cancelled by the user
    Cancelled,

    // The database connection could not be created or was lost
    BadConnection,

    // An internal error occurred in the engine.
    // Examples include:
    //  - Out of Memory
    //  - Filesystem full
    //  - Unknown crash
    //  - Filesystem permissions denied
    InternalException,
};

// Errors related to constraint validation
const ConstraintError = error{
    // A `NOT NULL` constraint was violated
    NotNullViolation,

    // A `FOREIGN KEY` constraint was violated
    ForeignKeyViolation,

    // A `UNIQUE` constraint was violated
    UniqueViolation,

    // A `CHECK` constraint was violated
    CheckViolation,

    // A unknown constraint type was violated
    ConstraintViolation,
};

// Errors related to argument binding
const ArgumentError = error{
    // One of the arguments passed could not be marshalled to pass to the SQL engine
    BindException,

    // The set of arguments passed did not map to query parameters
    UnusedArgument,

    // The allocator used for staging the query ran out of memory
    OutOfMemory,
    AllocatorRequired,
};

// Errors related to retrieving query result columns
const ResultColumnError = error{
    // The allocator used for retrieving the results ran out of memory
    OutOfMemory,
    AllocatorRequired,

    // A type error occurred when parsing results (means invalid data is in the DB)
    ResultTypeMismatch,
};

// Errors related to executing SQL queries
const StartQueryError = error{
    // The database is locked by another query and the timeout was exceeded
    DatabaseBusy,

    // Access to one or more resources was denied
    PermissionDenied,

    // The SQL query had invalid syntax or used an invalid identifier
    SqlException,

    // The set of columns to parse did not match the columns returned by the query
    ColumnMismatch,

    // Either an explicit transaction was open and a query method was called on the DB directly,
    // or no explicit transaction was open and a query method was called on a transaction;
    BadTransactionState,
};

pub const library_errors = struct {
    const BaseError = ConnectionError || UnexpectedError;

    // TODO: clean this up
    pub const OpenError = BaseError;
    pub const QueryError = BaseError || ArgumentError || ConstraintError || StartQueryError;
    pub const RowError = BaseError || ResultColumnError || ConstraintError || StartQueryError;
    pub const QueryRowError = QueryError || RowError || error{ NoRows, TooManyRows };
    pub const ExecError = QueryError;
    pub const BeginError = BaseError || StartQueryError;
    pub const CommitError = BaseError || StartQueryError || ConstraintError;
};
