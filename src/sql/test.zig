const sql = @import("./lib.zig");
const std = @import("std");
const Uuid = @import("util").Uuid;

const alloc = std.testing.allocator;

pub fn main() !void {
    const db = try sql.Db.open(.{
        //.postgres = .{
        //.conn_str = "postgresql://localhost",
        //},
        .sqlite = .{
            .file_path = "./test.db",
        },
    });
    defer db.close();

    const tx = try db.begin();
    try tx.commit();
    tx.rollback();
    try tx.commit();
}

test {
    const db = try sql.Db.open(.{
        .sqlite = .{
            .file_path = "./test.db",
        },
    });
    defer db.close();

    var results = try db.query(&.{[]const u8}, "SELECT $1 as id", .{"abcdefg"}, alloc);
    defer results.finish();

    const row = (try results.row(alloc)) orelse unreachable;
    defer alloc.free(row[0]);

    try std.testing.expectEqualStrings("abcdefg", row[0]);

    std.log.info("value: {s}", .{row[0]});
}
