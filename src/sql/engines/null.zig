const std = @import("std");
const common = @import("./common.zig");
const Allocator = std.mem.Allocator;

pub const Results = struct {
    pub fn row(_: *Results) common.RowError!?Row {
        unreachable;
    }
    pub fn columnCount(_: Results) common.ColumnCountError!u15 {
        unreachable;
    }
    pub fn columnIndex(_: Results, _: []const u8) common.ColumnIndexError!u15 {
        unreachable;
    }
    pub fn finish(_: Results) void {
        unreachable;
    }
};

pub const Row = struct {
    pub fn get(_: Row, comptime T: type, _: u15, _: ?Allocator) common.GetError!T {
        unreachable;
    }
};

pub const Db = struct {
    pub fn open(_: anytype) common.OpenError!Db {
        unreachable;
    }

    pub fn openUri(_: anytype) common.OpenError!Db {
        unreachable;
    }

    pub fn close(_: Db) void {
        unreachable;
    }

    pub fn exec(_: Db, _: [:0]const u8, _: anytype, _: common.QueryOptions) common.ExecError!Results {
        unreachable;
    }
};
