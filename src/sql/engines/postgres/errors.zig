const std = @import("std");
const c = @import("./c.zig");
const readVarInt = std.mem.readVarInt;

const Code = u40; // 8 * 5 = 40

pub const SqlStateClass = blk: {
    @setEvalBranchQuota(10000);
    const info = @typeInfo(SqlState).Enum;
    const EnumField = std.builtin.Type.EnumField;

    var fields: []const EnumField = &.{};
    for (info.fields) |field| {
        const class_code = toClassCode(field.value);
        if (class_code == field.value) {
            fields = fields ++ &[_]EnumField{field};
        }
    }
    break :blk @Type(.{ .Enum = .{
        .layout = info.layout,
        .tag_type = Code,
        .fields = fields,
        .decls = &.{},
        .is_exhaustive = false,
    } });
};

fn toCodeStr(code: Code) [5]u8 {
    var code_str: [5]u8 = undefined;
    code_str[0] = @intCast(u8, (code & 0xFF00_000000) >> 32);
    code_str[1] = @intCast(u8, (code & 0x00FF_000000) >> 24);
    code_str[2] = @intCast(u8, (code & 0x0000_FF0000) >> 16);
    code_str[3] = @intCast(u8, (code & 0x0000_00FF00) >> 8);
    code_str[4] = @intCast(u8, (code & 0x0000_0000FF) >> 0);

    return code_str;
}

fn toClassCode(code: Code) Code {
    var code_str = [_]u8{'0'} ** 5;
    code_str[0] = @intCast(u8, (code & 0xFF00_000000) >> 32);
    code_str[1] = @intCast(u8, (code & 0x00FF_000000) >> 24);

    code_str[2] = '0';
    code_str[3] = '0';
    code_str[4] = '0';

    return readVarInt(Code, &code_str, .Big);
}

// SqlState values for Postgres 14
pub const SqlState = blk: {
    @setEvalBranchQuota(10000);
    break :blk enum(Code) {
        pub const ParseError = error{ InvalidSize, NullPointer };
        pub fn parse(code_str: [*c]const u8) ParseError!SqlState {
            if (code_str == null) return error.NullPointer;
            const slice = std.mem.span(code_str);
            if (slice.len != @sizeOf(Code)) return error.InvalidSize;
            return @intToEnum(SqlState, readVarInt(Code, slice, .Big));
        }

        pub fn errorClass(code: SqlState) SqlStateClass {
            return @intToEnum(SqlStateClass, toClassCode(@enumToInt(code)));
        }

        pub fn errorCodeStr(code: SqlState) [5]u8 {
            return toCodeStr(@enumToInt(code));
        }

        // Class 00 — Successful Completion
        successful_completion = readVarInt(Code, "00000", .Big),
        // Class 01 — Warning
        warning = readVarInt(Code, "01000", .Big),
        dynamic_result_sets_returned = readVarInt(Code, "0100C", .Big),
        implicit_zero_bit_padding = readVarInt(Code, "01008", .Big),
        null_value_eliminated_in_set_function = readVarInt(Code, "01003", .Big),
        privilege_not_granted = readVarInt(Code, "01007", .Big),
        privilege_not_revoked = readVarInt(Code, "01006", .Big),
        string_data_right_truncation_warning = readVarInt(Code, "01004", .Big),
        deprecated_feature = readVarInt(Code, "01P01", .Big),
        // Class 02 — No Data (this is also a warning class per the SQL standard)
        no_data = readVarInt(Code, "02000", .Big),
        no_additional_dynamic_result_sets_returned = readVarInt(Code, "02001", .Big),
        // Class 03 — SQL Statement Not Yet Complete
        sql_statement_not_yet_complete = readVarInt(Code, "03000", .Big),
        // Class 08 — Connection Exception
        connection_exception = readVarInt(Code, "08000", .Big),
        connection_does_not_exist = readVarInt(Code, "08003", .Big),
        connection_failure = readVarInt(Code, "08006", .Big),
        sqlclient_unable_to_establish_sqlconnection = readVarInt(Code, "08001", .Big),
        sqlserver_rejected_establishment_of_sqlconnection = readVarInt(Code, "08004", .Big),
        transaction_resolution_unknown = readVarInt(Code, "08007", .Big),
        protocol_violation = readVarInt(Code, "08P01", .Big),
        // Class 09 — Triggered Action Exception
        triggered_action_exception = readVarInt(Code, "09000", .Big),
        // Class 0A — Feature Not Supported
        feature_not_supported = readVarInt(Code, "0A000", .Big),
        // Class 0B — Invalid Transaction Initiation
        invalid_transaction_initiation = readVarInt(Code, "0B000", .Big),
        // Class 0F — Locator Exception
        locator_exception = readVarInt(Code, "0F000", .Big),
        invalid_locator_specification = readVarInt(Code, "0F001", .Big),
        // Class 0L — Invalid Grantor
        invalid_grantor = readVarInt(Code, "0L000", .Big),
        invalid_grant_operation = readVarInt(Code, "0LP01", .Big),
        // Class 0P — Invalid Role Specification
        invalid_role_specification = readVarInt(Code, "0P000", .Big),
        // Class 0Z — Diagnostics Exception
        diagnostics_exception = readVarInt(Code, "0Z000", .Big),
        stacked_diagnostics_accessed_without_active_handler = readVarInt(Code, "0Z002", .Big),
        // Class 20 — Case Not Found
        case_not_found = readVarInt(Code, "20000", .Big),
        // Class 21 — Cardinality Violation
        cardinality_violation = readVarInt(Code, "21000", .Big),
        // Class 22 — Data Exception
        data_exception = readVarInt(Code, "22000", .Big),
        array_subscript_error = readVarInt(Code, "2202E", .Big),
        character_not_in_repertoire = readVarInt(Code, "22021", .Big),
        datetime_field_overflow = readVarInt(Code, "22008", .Big),
        division_by_zero = readVarInt(Code, "22012", .Big),
        error_in_assignment = readVarInt(Code, "22005", .Big),
        escape_character_conflict = readVarInt(Code, "2200B", .Big),
        indicator_overflow = readVarInt(Code, "22022", .Big),
        interval_field_overflow = readVarInt(Code, "22015", .Big),
        invalid_argument_for_logarithm = readVarInt(Code, "2201E", .Big),
        invalid_argument_for_ntile_function = readVarInt(Code, "22014", .Big),
        invalid_argument_for_nth_value_function = readVarInt(Code, "22016", .Big),
        invalid_argument_for_power_function = readVarInt(Code, "2201F", .Big),
        invalid_argument_for_width_bucket_function = readVarInt(Code, "2201G", .Big),
        invalid_character_value_for_cast = readVarInt(Code, "22018", .Big),
        invalid_datetime_format = readVarInt(Code, "22007", .Big),
        invalid_escape_character = readVarInt(Code, "22019", .Big),
        invalid_escape_octet = readVarInt(Code, "2200D", .Big),
        invalid_escape_sequence = readVarInt(Code, "22025", .Big),
        nonstandard_use_of_escape_character = readVarInt(Code, "22P06", .Big),
        invalid_indicator_parameter_value = readVarInt(Code, "22010", .Big),
        invalid_parameter_value = readVarInt(Code, "22023", .Big),
        invalid_preceding_or_following_size = readVarInt(Code, "22013", .Big),
        invalid_regular_expression = readVarInt(Code, "2201B", .Big),
        invalid_row_count_in_limit_clause = readVarInt(Code, "2201W", .Big),
        invalid_row_count_in_result_offset_clause = readVarInt(Code, "2201X", .Big),
        invalid_tablesample_argument = readVarInt(Code, "2202H", .Big),
        invalid_tablesample_repeat = readVarInt(Code, "2202G", .Big),
        invalid_time_zone_displacement_value = readVarInt(Code, "22009", .Big),
        invalid_use_of_escape_character = readVarInt(Code, "2200C", .Big),
        most_specific_type_mismatch = readVarInt(Code, "2200G", .Big),
        null_value_not_allowed_data_exception = readVarInt(Code, "22004", .Big),
        null_value_no_indicator_parameter = readVarInt(Code, "22002", .Big),
        numeric_value_out_of_range = readVarInt(Code, "22003", .Big),
        sequence_generator_limit_exceeded = readVarInt(Code, "2200H", .Big),
        string_data_length_mismatch = readVarInt(Code, "22026", .Big),
        string_data_right_truncation_exception = readVarInt(Code, "22001", .Big),
        substring_error = readVarInt(Code, "22011", .Big),
        trim_error = readVarInt(Code, "22027", .Big),
        unterminated_c_string = readVarInt(Code, "22024", .Big),
        zero_length_character_string = readVarInt(Code, "2200F", .Big),
        floating_point_exception = readVarInt(Code, "22P01", .Big),
        invalid_text_representation = readVarInt(Code, "22P02", .Big),
        invalid_binary_representation = readVarInt(Code, "22P03", .Big),
        bad_copy_file_format = readVarInt(Code, "22P04", .Big),
        untranslatable_character = readVarInt(Code, "22P05", .Big),
        not_an_xml_document = readVarInt(Code, "2200L", .Big),
        invalid_xml_document = readVarInt(Code, "2200M", .Big),
        invalid_xml_content = readVarInt(Code, "2200N", .Big),
        invalid_xml_comment = readVarInt(Code, "2200S", .Big),
        invalid_xml_processing_instruction = readVarInt(Code, "2200T", .Big),
        duplicate_json_object_key_value = readVarInt(Code, "22030", .Big),
        invalid_argument_for_sql_json_datetime_function = readVarInt(Code, "22031", .Big),
        invalid_json_text = readVarInt(Code, "22032", .Big),
        invalid_sql_json_subscript = readVarInt(Code, "22033", .Big),
        more_than_one_sql_json_item = readVarInt(Code, "22034", .Big),
        no_sql_json_item = readVarInt(Code, "22035", .Big),
        non_numeric_sql_json_item = readVarInt(Code, "22036", .Big),
        non_unique_keys_in_a_json_object = readVarInt(Code, "22037", .Big),
        singleton_sql_json_item_required = readVarInt(Code, "22038", .Big),
        sql_json_array_not_found = readVarInt(Code, "22039", .Big),
        sql_json_member_not_found = readVarInt(Code, "2203A", .Big),
        sql_json_number_not_found = readVarInt(Code, "2203B", .Big),
        sql_json_object_not_found = readVarInt(Code, "2203C", .Big),
        too_many_json_array_elements = readVarInt(Code, "2203D", .Big),
        too_many_json_object_members = readVarInt(Code, "2203E", .Big),
        sql_json_scalar_required = readVarInt(Code, "2203F", .Big),
        // Class 23 — Integrity Constraint Violation
        integrity_constraint_violation = readVarInt(Code, "23000", .Big),
        restrict_violation = readVarInt(Code, "23001", .Big),
        not_null_violation = readVarInt(Code, "23502", .Big),
        foreign_key_violation = readVarInt(Code, "23503", .Big),
        unique_violation = readVarInt(Code, "23505", .Big),
        check_violation = readVarInt(Code, "23514", .Big),
        exclusion_violation = readVarInt(Code, "23P01", .Big),
        // Class 24 — Invalid Cursor State
        invalid_cursor_state = readVarInt(Code, "24000", .Big),
        // Class 25 — Invalid Transaction State
        invalid_transaction_state = readVarInt(Code, "25000", .Big),
        active_sql_transaction = readVarInt(Code, "25001", .Big),
        branch_transaction_already_active = readVarInt(Code, "25002", .Big),
        held_cursor_requires_same_isolation_level = readVarInt(Code, "25008", .Big),
        inappropriate_access_mode_for_branch_transaction = readVarInt(Code, "25003", .Big),
        inappropriate_isolation_level_for_branch_transaction = readVarInt(Code, "25004", .Big),
        no_active_sql_transaction_for_branch_transaction = readVarInt(Code, "25005", .Big),
        read_only_sql_transaction = readVarInt(Code, "25006", .Big),
        schema_and_data_statement_mixing_not_supported = readVarInt(Code, "25007", .Big),
        no_active_sql_transaction = readVarInt(Code, "25P01", .Big),
        in_failed_sql_transaction = readVarInt(Code, "25P02", .Big),
        idle_in_transaction_session_timeout = readVarInt(Code, "25P03", .Big),
        // Class 26 — Invalid SQL Statement Name
        invalid_sql_statement_name = readVarInt(Code, "26000", .Big),
        // Class 27 — Triggered Data Change Violation
        triggered_data_change_violation = readVarInt(Code, "27000", .Big),
        // Class 28 — Invalid Authorization Specification
        invalid_authorization_specification = readVarInt(Code, "28000", .Big),
        invalid_password = readVarInt(Code, "28P01", .Big),
        // Class 2B — Dependent Privilege Descriptors Still Exist
        dependent_privilege_descriptors_still_exist = readVarInt(Code, "2B000", .Big),
        dependent_objects_still_exist = readVarInt(Code, "2BP01", .Big),
        // Class 2D — Invalid Transaction Termination
        invalid_transaction_termination = readVarInt(Code, "2D000", .Big),
        // Class 2F — SQL Routine Exception
        sql_routine_exception = readVarInt(Code, "2F000", .Big),
        function_executed_no_return_statement = readVarInt(Code, "2F005", .Big),
        modifying_sql_data_not_permitted_sql_exception = readVarInt(Code, "2F002", .Big),
        prohibited_sql_statement_attempted_sql_exception = readVarInt(Code, "2F003", .Big),
        reading_sql_data_not_permitted_sql_exception = readVarInt(Code, "2F004", .Big),
        // Class 34 — Invalid Cursor Name
        invalid_cursor_name = readVarInt(Code, "34000", .Big),
        // Class 38 — External Routine Exception
        external_routine_exception = readVarInt(Code, "38000", .Big),
        containing_sql_not_permitted = readVarInt(Code, "38001", .Big),
        modifying_sql_data_not_permitted_external_exception = readVarInt(Code, "38002", .Big),
        prohibited_sql_statement_attempted_external_exception = readVarInt(Code, "38003", .Big),
        reading_sql_data_not_permitted_external_exception = readVarInt(Code, "38004", .Big),
        // Class 39 — External Routine Invocation Exception
        external_routine_invocation_exception = readVarInt(Code, "39000", .Big),
        invalid_sqlstate_returned = readVarInt(Code, "39001", .Big),
        null_value_not_allowed_external_exception = readVarInt(Code, "39004", .Big),
        trigger_protocol_violated = readVarInt(Code, "39P01", .Big),
        srf_protocol_violated = readVarInt(Code, "39P02", .Big),
        event_trigger_protocol_violated = readVarInt(Code, "39P03", .Big),
        // Class 3B — Savepoint Exception
        savepoint_exception = readVarInt(Code, "3B000", .Big),
        invalid_savepoint_specification = readVarInt(Code, "3B001", .Big),
        // Class 3D — Invalid Catalog Name
        invalid_catalog_name = readVarInt(Code, "3D000", .Big),
        // Class 3F — Invalid Schema Name
        invalid_schema_name = readVarInt(Code, "3F000", .Big),
        // Class 40 — Transaction Rollback
        transaction_rollback = readVarInt(Code, "40000", .Big),
        transaction_integrity_constraint_violation = readVarInt(Code, "40002", .Big),
        serialization_failure = readVarInt(Code, "40001", .Big),
        statement_completion_unknown = readVarInt(Code, "40003", .Big),
        deadlock_detected = readVarInt(Code, "40P01", .Big),
        // Class 42 — Syntax Error or Access Rule Violation
        syntax_error_or_access_rule_violation = readVarInt(Code, "42000", .Big),
        syntax_error = readVarInt(Code, "42601", .Big),
        insufficient_privilege = readVarInt(Code, "42501", .Big),
        cannot_coerce = readVarInt(Code, "42846", .Big),
        grouping_error = readVarInt(Code, "42803", .Big),
        windowing_error = readVarInt(Code, "42P20", .Big),
        invalid_recursion = readVarInt(Code, "42P19", .Big),
        invalid_foreign_key = readVarInt(Code, "42830", .Big),
        invalid_name = readVarInt(Code, "42602", .Big),
        name_too_long = readVarInt(Code, "42622", .Big),
        reserved_name = readVarInt(Code, "42939", .Big),
        datatype_mismatch = readVarInt(Code, "42804", .Big),
        indeterminate_datatype = readVarInt(Code, "42P18", .Big),
        collation_mismatch = readVarInt(Code, "42P21", .Big),
        indeterminate_collation = readVarInt(Code, "42P22", .Big),
        wrong_object_type = readVarInt(Code, "42809", .Big),
        generated_always = readVarInt(Code, "428C9", .Big),
        undefined_column = readVarInt(Code, "42703", .Big),
        undefined_function = readVarInt(Code, "42883", .Big),
        undefined_table = readVarInt(Code, "42P01", .Big),
        undefined_parameter = readVarInt(Code, "42P02", .Big),
        undefined_object = readVarInt(Code, "42704", .Big),
        duplicate_column = readVarInt(Code, "42701", .Big),
        duplicate_cursor = readVarInt(Code, "42P03", .Big),
        duplicate_database = readVarInt(Code, "42P04", .Big),
        duplicate_function = readVarInt(Code, "42723", .Big),
        duplicate_prepared_statement = readVarInt(Code, "42P05", .Big),
        duplicate_schema = readVarInt(Code, "42P06", .Big),
        duplicate_table = readVarInt(Code, "42P07", .Big),
        duplicate_alias = readVarInt(Code, "42712", .Big),
        duplicate_object = readVarInt(Code, "42710", .Big),
        ambiguous_column = readVarInt(Code, "42702", .Big),
        ambiguous_function = readVarInt(Code, "42725", .Big),
        ambiguous_parameter = readVarInt(Code, "42P08", .Big),
        ambiguous_alias = readVarInt(Code, "42P09", .Big),
        invalid_column_reference = readVarInt(Code, "42P10", .Big),
        invalid_column_definition = readVarInt(Code, "42611", .Big),
        invalid_cursor_definition = readVarInt(Code, "42P11", .Big),
        invalid_database_definition = readVarInt(Code, "42P12", .Big),
        invalid_function_definition = readVarInt(Code, "42P13", .Big),
        invalid_prepared_statement_definition = readVarInt(Code, "42P14", .Big),
        invalid_schema_definition = readVarInt(Code, "42P15", .Big),
        invalid_table_definition = readVarInt(Code, "42P16", .Big),
        invalid_object_definition = readVarInt(Code, "42P17", .Big),
        // Class 44 — WITH CHECK OPTION Violation
        with_check_option_violation = readVarInt(Code, "44000", .Big),
        // Class 53 — Insufficient Resources
        insufficient_resources = readVarInt(Code, "53000", .Big),
        disk_full = readVarInt(Code, "53100", .Big),
        out_of_memory = readVarInt(Code, "53200", .Big),
        too_many_connections = readVarInt(Code, "53300", .Big),
        configuration_limit_exceeded = readVarInt(Code, "53400", .Big),
        // Class 54 — Program Limit Exceeded
        program_limit_exceeded = readVarInt(Code, "54000", .Big),
        statement_too_complex = readVarInt(Code, "54001", .Big),
        too_many_columns = readVarInt(Code, "54011", .Big),
        too_many_arguments = readVarInt(Code, "54023", .Big),
        // Class 55 — Object Not In Prerequisite State
        object_not_in_prerequisite_state = readVarInt(Code, "55000", .Big),
        object_in_use = readVarInt(Code, "55006", .Big),
        cant_change_runtime_param = readVarInt(Code, "55P02", .Big),
        lock_not_available = readVarInt(Code, "55P03", .Big),
        unsafe_new_enum_value_usage = readVarInt(Code, "55P04", .Big),
        // Class 57 — Operator Intervention
        operator_intervention = readVarInt(Code, "57000", .Big),
        query_canceled = readVarInt(Code, "57014", .Big),
        admin_shutdown = readVarInt(Code, "57P01", .Big),
        crash_shutdown = readVarInt(Code, "57P02", .Big),
        cannot_connect_now = readVarInt(Code, "57P03", .Big),
        database_dropped = readVarInt(Code, "57P04", .Big),
        idle_session_timeout = readVarInt(Code, "57P05", .Big),
        // Class 58 — System Error (errors external to PostgreSQL itself)
        system_error = readVarInt(Code, "58000", .Big),
        io_error = readVarInt(Code, "58030", .Big),
        undefined_file = readVarInt(Code, "58P01", .Big),
        duplicate_file = readVarInt(Code, "58P02", .Big),
        // Class 72 — Snapshot Failure
        snapshot_too_old = readVarInt(Code, "72000", .Big),
        // Class F0 — Configuration File Error
        config_file_error = readVarInt(Code, "F0000", .Big),
        lock_file_exists = readVarInt(Code, "F0001", .Big),
        // Class HV — Foreign Data Wrapper Error (SQL/MED)
        fdw_error = readVarInt(Code, "HV000", .Big),
        fdw_column_name_not_found = readVarInt(Code, "HV005", .Big),
        fdw_dynamic_parameter_value_needed = readVarInt(Code, "HV002", .Big),
        fdw_function_sequence_error = readVarInt(Code, "HV010", .Big),
        fdw_inconsistent_descriptor_information = readVarInt(Code, "HV021", .Big),
        fdw_invalid_attribute_value = readVarInt(Code, "HV024", .Big),
        fdw_invalid_column_name = readVarInt(Code, "HV007", .Big),
        fdw_invalid_column_number = readVarInt(Code, "HV008", .Big),
        fdw_invalid_data_type = readVarInt(Code, "HV004", .Big),
        fdw_invalid_data_type_descriptors = readVarInt(Code, "HV006", .Big),
        fdw_invalid_descriptor_field_identifier = readVarInt(Code, "HV091", .Big),
        fdw_invalid_handle = readVarInt(Code, "HV00B", .Big),
        fdw_invalid_option_index = readVarInt(Code, "HV00C", .Big),
        fdw_invalid_option_name = readVarInt(Code, "HV00D", .Big),
        fdw_invalid_string_length_or_buffer_length = readVarInt(Code, "HV090", .Big),
        fdw_invalid_string_format = readVarInt(Code, "HV00A", .Big),
        fdw_invalid_use_of_null_pointer = readVarInt(Code, "HV009", .Big),
        fdw_too_many_handles = readVarInt(Code, "HV014", .Big),
        fdw_out_of_memory = readVarInt(Code, "HV001", .Big),
        fdw_no_schemas = readVarInt(Code, "HV00P", .Big),
        fdw_option_name_not_found = readVarInt(Code, "HV00J", .Big),
        fdw_reply_handle = readVarInt(Code, "HV00K", .Big),
        fdw_schema_not_found = readVarInt(Code, "HV00Q", .Big),
        fdw_table_not_found = readVarInt(Code, "HV00R", .Big),
        fdw_unable_to_create_execution = readVarInt(Code, "HV00L", .Big),
        fdw_unable_to_create_reply = readVarInt(Code, "HV00M", .Big),
        fdw_unable_to_establish_connection = readVarInt(Code, "HV00N", .Big),
        // Class P0 — PL/pgSQL Error
        plpgsql_error = readVarInt(Code, "P0000", .Big),
        raise_exception = readVarInt(Code, "P0001", .Big),
        no_data_found = readVarInt(Code, "P0002", .Big),
        too_many_rows = readVarInt(Code, "P0003", .Big),
        assert_failure = readVarInt(Code, "P0004", .Big),
        // Class XX — Internal Error
        internal_error = readVarInt(Code, "XX000", .Big),
        data_corrupted = readVarInt(Code, "XX001", .Big),
        index_corrupted = readVarInt(Code, "XX002", .Big),

        _,
    };
};
