const std = @import("std");
const Pkg = std.build.Pkg;

const Packages = struct {
    opts: Pkg,
    util: Pkg,
    http: Pkg,
    sql: Pkg,
    api: Pkg,
    template: Pkg,
    main: Pkg,
};

fn makePkgs(b: *std.build.Builder, build_options: Pkg) Packages {
    const opt = b.dupePkg(build_options);
    const util_pkg = b.dupePkg(.{
        .name = "util",
        .source = std.build.FileSource.relative("src/util/lib.zig"),
        .dependencies = &.{opt},
    });

    const http_pkg = b.dupePkg(.{
        .name = "http",
        .source = std.build.FileSource.relative("src/http/lib.zig"),
        .dependencies = &.{ util_pkg, opt },
    });

    const sql_pkg = b.dupePkg(.{
        .name = "sql",
        .source = std.build.FileSource.relative("src/sql/lib.zig"),
        .dependencies = &.{ util_pkg, opt },
    });

    const api_pkg = b.dupePkg(.{
        .name = "api",
        .source = std.build.FileSource.relative("src/api/lib.zig"),
        .dependencies = &.{ util_pkg, sql_pkg, opt },
    });

    const template_pkg = b.dupePkg(.{
        .name = "template",
        .source = std.build.FileSource.relative("src/template/lib.zig"),
        .dependencies = &.{opt},
    });

    const main_pkg = b.dupePkg(.{
        .name = "main",
        .source = std.build.FileSource.relative("src/main/main.zig"),
        .dependencies = &.{ util_pkg, http_pkg, sql_pkg, api_pkg, template_pkg, opt },
    });

    return Packages{
        .opts = opt,
        .util = util_pkg,
        .http = http_pkg,
        .sql = sql_pkg,
        .api = api_pkg,
        .template = template_pkg,
        .main = main_pkg,
    };
}

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();
    const options = b.addOptions();
    const enable_sqlite = b.option(bool, "sqlite", "Enable the use of sqlite as a database engine") orelse true;
    const enable_postgres = b.option(bool, "postgres", "Enable the use of postgres as a database engine") orelse true;
    options.addOption(bool, "enable_sqlite", enable_sqlite);
    options.addOption(bool, "enable_postgres", enable_postgres);
    if (!enable_sqlite and !enable_postgres) {
        std.log.err("No database engine enabled", .{});
        return error.InvalidBuildOptions;
    }

    const pkgs = makePkgs(b, options.getPackage("build_options"));

    const exe = b.addExecutable("fediglam", "src/main/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);

    exe.addPackage(pkgs.opts);
    exe.addPackage(pkgs.sql);
    exe.addPackage(pkgs.util);
    exe.addPackage(pkgs.http);
    exe.addPackage(pkgs.api);
    exe.addPackage(pkgs.template);

    if (enable_sqlite) exe.linkSystemLibrary("sqlite3");
    if (enable_postgres) exe.linkSystemLibrary("pq");
    exe.linkLibC();
    exe.addSystemIncludePath("/usr/include/");
    exe.addSystemIncludePath("/usr/include/postgresql"); // HACK

    const unittest_options = b.addOptions();
    unittest_options.addOption(bool, "enable_sqlite", false);
    unittest_options.addOption(bool, "enable_postgres", false);

    const unittest_pkgs = makePkgs(b, unittest_options.getPackage("build_options"));

    const unittest_http_cmd = b.step("unit:http", "Run tests for http package");
    const unittest_http = b.addTest("src/http/lib.zig");
    unittest_http_cmd.dependOn(&unittest_http.step);
    unittest_http.addPackage(unittest_pkgs.util);

    const unittest_util_cmd = b.step("unit:util", "Run tests for util package");
    const unittest_util = b.addTest("src/util/lib.zig");
    unittest_util_cmd.dependOn(&unittest_util.step);

    const unittest_sql_cmd = b.step("unit:sql", "Run tests for sql package");
    const unittest_sql = b.addTest("src/sql/lib.zig");
    unittest_sql_cmd.dependOn(&unittest_sql.step);
    unittest_sql.addPackage(unittest_pkgs.util);
    //unittest_sql.linkLibC();

    const unittest_template_cmd = b.step("unit:template", "Run tests for template package");
    const unittest_template = b.addTest("src/template/lib.zig");
    unittest_template_cmd.dependOn(&unittest_template.step);

    const unittest_api_cmd = b.step("unit:api", "Run tests for api package");
    const unittest_api = b.addTest("src/api/lib.zig");
    unittest_api_cmd.dependOn(&unittest_api.step);
    unittest_api.addPackage(unittest_pkgs.util);
    unittest_api.addPackage(unittest_pkgs.sql);
    //unittest_api.linkLibC();

    //const util_tests = b.addTest("src/util/lib.zig");
    //const sql_tests = b.addTest("src/sql/lib.zig");
    //http_tests.addPackage(pkgs.util);
    //sql_tests.addPackage(pkgs.util);

    //const unit_tests = b.step("unit-tests", "Run tests");
    const unittest_all = b.step("unit", "Run unit tests");
    unittest_all.dependOn(unittest_http_cmd);
    unittest_all.dependOn(unittest_util_cmd);
    unittest_all.dependOn(unittest_sql_cmd);
    unittest_all.dependOn(unittest_template_cmd);
    unittest_all.dependOn(unittest_api_cmd);

    const api_integration = b.addTest("./tests/api_integration/lib.zig");
    api_integration.addPackage(pkgs.opts);
    api_integration.addPackage(pkgs.sql);
    api_integration.addPackage(pkgs.util);
    api_integration.addPackage(pkgs.http);
    api_integration.addPackage(pkgs.main);
    api_integration.addPackage(pkgs.api);
    api_integration.linkLibC();
    api_integration.addSystemIncludePath("/usr/include/");
    api_integration.addSystemIncludePath("/usr/include/postgresql"); // HACK
    if (enable_sqlite) api_integration.linkSystemLibrary("sqlite3");
    if (enable_postgres) api_integration.linkSystemLibrary("pq");

    const integration_tests = b.step("integration-tests", "run tests");
    integration_tests.dependOn(&api_integration.step);

    const run_cmd = exe.run();
    //run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the server");
    run_step.dependOn(&run_cmd.step);
}
